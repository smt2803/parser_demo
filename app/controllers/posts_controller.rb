class PostsController < ApplicationController
  before_action :habr_parser, only: [:index]

  def index
    @posts = Parser.all.take(10)

  end

  private

  def habr_parser
    Parser.delay.parser
  end

end
