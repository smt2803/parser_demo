class Parser < ActiveRecord::Base

  require 'open-uri'
  require 'nokogiri'

  default_scope { order(created_at: :desc) }

  def self.parser
    data = []
    source = 'https://habrahabr.ru/'
    agent = 'firefox'

    page = Nokogiri::HTML(open(source, 'User-Agent' => agent))

    page.css('h1.title a.post_title').each do |link|
      data << [link['href'], link.children.text]
    end

    data.reverse_each do |value|
      result = Parser.where(url: value.first)

      if result.blank?
        Parser.create(title: value.last, url: value.first)
      end
    end
  end

end
